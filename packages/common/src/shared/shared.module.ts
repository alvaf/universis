import {CUSTOM_ELEMENTS_SCHEMA, ModuleWithProviders, NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {TranslateModule} from '@ngx-translate/core';
import {NgSpinKitModule} from 'ng-spin-kit';
import {LocalizedDatePipe} from './pipes/localized-date.pipe';
import {MsgboxComponent} from './components/msgbox/msgbox.component';
import {DialogComponent} from './components/modal/dialog.component';
import {SpinnerComponent} from './components/modal/spinner.component';
import {APP_CONFIGURATION, ApplicationConfiguration, ConfigurationService} from './services/configuration.service';
import {ModalService} from './services/modal.service';
import {LoadingService} from './services/loading.service';
import {HttpClientModule} from '@angular/common/http';

@NgModule({
    imports: [
        CommonModule,
        HttpClientModule,
        TranslateModule,
        NgSpinKitModule,
    ],
    declarations: [
        LocalizedDatePipe,
        MsgboxComponent,
        DialogComponent,
        SpinnerComponent],
    entryComponents: [
        DialogComponent,
        SpinnerComponent
    ],
    providers: [
        {
            provide: APP_CONFIGURATION,
            useValue: <ApplicationConfiguration>{
                settings: {
                    remote: {
                      server: '/'
                    },
                    i18n: {
                        locales: [ 'en' ],
                        defaultLocale: 'en'
                    }
                }
            }
        },
        ConfigurationService,
        ModalService,
        LoadingService
    ],
    exports: [
        LocalizedDatePipe,
        MsgboxComponent,
        DialogComponent,
        SpinnerComponent],
    schemas: [
        CUSTOM_ELEMENTS_SCHEMA
    ]
})
export class SharedModule {

    static forRoot(): ModuleWithProviders {
        return {
            ngModule: SharedModule,
            providers: [
                ConfigurationService,
                ModalService,
                LoadingService
            ]
        };
    }
}
