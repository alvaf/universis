import { Injectable } from '@angular/core';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { SpinnerComponent } from '../components/modal/spinner.component';

/**
 *
 * Displays Loading Spinner
 * @export
 * @class LoadingService
 */
@Injectable()
export class LoadingService {
  // @ts-ignore
  private modalRef: BsModalRef;
  // @ts-ignore
  isEnabled: boolean;

  config = {
    ignoreBackdropClick: true,
    animated: false,
    keyboard: false,
    class: 'modal-dialog-centered'
  };

  constructor(private modalService: BsModalService) {
  }

  /**
   *
   * Toggle show/hide state of Spinner
   * @memberof LoadingService
   */
  toggle() {
    if (this.isEnabled) {
      this.hideLoading();
    } else {
      this.showLoading();
    }
  }

  /**
   *
   * Show Spinner
   * @memberof LoadingService
   */
  showLoading() {
    if (!this.isEnabled) {
      this.modalRef = this.modalService.show(SpinnerComponent, this.config);
      this.isEnabled = true;
    }
  }

  /**
   *
   * Hide Spinner
   * @memberof LoadingService
   */
  hideLoading() {
    if (this.isEnabled) {
      this.modalRef.hide();
      this.isEnabled = false;
    }
  }
}
