import {CUSTOM_ELEMENTS_SCHEMA, NgModule, OnInit} from '@angular/core';
import {DashboardComponent} from './dashboard.component';
import {DashboardRoutingModule} from './dashboard-routing.module';
import {TranslateModule, TranslateService} from '@ngx-translate/core';
import {environment} from '../../environments/environment';
import {CommonModule} from '@angular/common';
import {AppSidebarService} from '../registrar-shared/services/app-sidebar.service';

@NgModule({
    imports: [
        CommonModule,
        DashboardRoutingModule,
        TranslateModule
    ],
    declarations: [DashboardComponent],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class DashboardModule implements OnInit {
    constructor(private _translateService: TranslateService,
                private _appSidebarService: AppSidebarService) {
        this.ngOnInit().catch(err => {
            console.error('An error occurred while loading dashboard module');
            console.error(err);
        });
    }

    async ngOnInit() {
        // create promises chain
        const sources = environment.languages.map(async (language) => {
            const translations = await import(`./i18n/dashboard.${language}.json`);
            this._translateService.setTranslation(language, translations, true);
        });
        // execute chain
        await Promise.all(sources);
    }
}
